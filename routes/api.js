var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.send('Api v1.0');
});

router.get('/items', (req, res) => {
  const items = [{id: '1', name: 'fruit'}, {id: '2', name: 'viande'}];

  res.send(items);
})

module.exports = router;
