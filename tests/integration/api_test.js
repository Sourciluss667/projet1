const app = require("../../app");
const request = require('supertest');
const { expect } = require("chai");
const { exec } = require("child_process");

function connectToExpress () {
  const agent = request.agent(app);
  return agent;
}


describe('GET /api/items test.', () => {
    it('should return all items', async () => {
      const agent = connectToExpress();

      return agent.get('/api/items')
      .expect(200)
      .expect('Content-Type', /json/)
      .then(res => {
        expect(res.body)
        .to.be.an('array')
        .of.length(2)

        res.body.forEach(element => {
          expect(element).to.have.property('id');
          expect(element).to.have.property('name');
        });
      })
    })
})